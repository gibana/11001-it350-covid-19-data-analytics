import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionDeltaComponent } from './section-delta.component';

describe('SectionDeltaComponent', () => {
  let component: SectionDeltaComponent;
  let fixture: ComponentFixture<SectionDeltaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionDeltaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionDeltaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
