import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { 
  ngxChartsBarModule, 
  ngxChartsLineModule, 
  ngxChartsComboModule, 
  ngxChartsPieModule, 
  ngxChartsStackedModule 
} from '@tusharghoshbd/ngx-charts';

import { DataService } from '../services/DataService'

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SectionAlphaComponent } from './section-alpha/section-alpha.component';
import { SectionBetaComponent } from './section-beta/section-beta.component';
import { SectionCharlieComponent } from './section-charlie/section-charlie.component';
import { SectionDeltaComponent } from './section-delta/section-delta.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: '**', component: DashboardComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SectionAlphaComponent,
    SectionBetaComponent,
    SectionCharlieComponent,
    SectionDeltaComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    ngxChartsBarModule,
    ngxChartsLineModule,
    ngxChartsComboModule,
    ngxChartsPieModule,
    ngxChartsStackedModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
