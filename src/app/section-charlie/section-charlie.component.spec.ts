import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionCharlieComponent } from './section-charlie.component';

describe('SectionCharlieComponent', () => {
  let component: SectionCharlieComponent;
  let fixture: ComponentFixture<SectionCharlieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionCharlieComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionCharlieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
