import { Component, OnInit, Input } from '@angular/core';
import { Utils } from '../../utils/Utils';

@Component({
  selector: 'section-charlie',
  templateUrl: './section-charlie.component.html',
  styleUrls: ['./section-charlie.component.css']
})
export class SectionCharlieComponent implements OnInit {

  @Input() data: any = [];
  @Input() dateRange: any = [];
  @Input() countries: any = [];

  filterCountries: string = '';
  
  rangeMin: number = 0;
  rangeMax: number = 10;
  filterDateRange: number = 0;
  filterInputDate: any;
  filterDate: any;

  dataViewConfirm: any = [];
  dataViewDeath: any = [];
  dataViewVaccinated: any = [];

  view: any[] = [1000, 600];
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = false;
  showLegend: boolean = true;
  showXAxisLabel: boolean = true;
  showYAxisLabel: boolean = true;
  xScaleMax: number = 20000;

  constructor() { }

  ngOnInit(): void {
    this.setFilters();
    this.setFilterCountry();
    this.handleRangeChange();
    let _this = this;
    setTimeout(function(){
      _this.changeSelectAll();
      _this.changeFilters();
      _this.xScaleMax = 500;
    }, 3000);
  }

  getData() {
    let _this = this;
    let temp = Object.assign([], this.data);
    this.dataViewConfirm = [];
    this.dataViewDeath = [];
    this.dataViewVaccinated = [];
    let confirmAsia = [];
    let confirmAfrica = [];
    let confirmNorthAmerica = [];
    let confirmSouthAmerica = [];
    let confirmEurope = [];
    let confirmOceania = [];
    let confirmContinent = [];
    let deathAsia = [];
    let deathAfrica = [];
    let deathNorthAmerica = [];
    let deathSouthAmerica = [];
    let deathEurope = [];
    let deathOceania = [];
    let deathContinent = [];

    let vaccinatedAsia = [];
    let vaccinatedAfrica = [];
    let vaccinatedNorthAmerica = [];
    let vaccinatedSouthAmerica = [];
    let vaccinatedEurope = [];
    let vaccinatedOceania = [];
    let vaccinatedContinent = [];

    for(let index = 0; index < temp.length; index++) {
      let value = temp[index];
      if(!this.filterCountries.includes('[' + value['location'] +']')) continue;
      if(Date.parse(value['date']) == Date.parse(_this.filterDate)) {
        let confirm = {
          name: value['location'],
          x: parseInt(value['population_density']),
          y: parseInt(value['total_cases_per_million']),
          r: 10
        };
        let death = {
          name: value['location'],
          x: parseInt(value['population_density']),
          y: parseInt(value['total_deaths_per_million']),
          r: 10
        };
        let vaccinated = {
          name: value['location'],
          x: parseInt(value['population_density']),
          y: parseInt(value['total_vaccinations_per_hundred']),
          r: 10
        };
        if(value['continent'] === 'Asia') {
          if(value['total_cases_per_million'] > 0) confirmAsia.push(confirm); 
          if(value['total_deaths_per_million'] > 0) deathAsia.push(death); 
          if(value['total_vaccinations_per_hundred'] > 0) vaccinatedAsia.push(vaccinated); 
        }
        else if(value['continent'] === 'Africa') {
          if(value['total_cases_per_million'] > 0) confirmAfrica.push(confirm);
          if(value['total_deaths_per_million'] > 0) deathAfrica.push(death);
          if(value['total_vaccinations_per_hundred'] > 0) vaccinatedAfrica.push(vaccinated);
        }
        else if(value['continent'] === 'North America') {
          if(value['total_cases_per_million'] > 0) confirmNorthAmerica.push(confirm);
          if(value['total_deaths_per_million'] > 0) deathNorthAmerica.push(death);
          if(value['total_vaccinations_per_hundred'] > 0) vaccinatedNorthAmerica.push(vaccinated);
        }
        else if(value['continent'] === 'South America') {
          if(value['total_cases_per_million'] > 0) confirmSouthAmerica.push(confirm);
          if(value['total_deaths_per_million'] > 0) deathSouthAmerica.push(death);
          if(value['total_vaccinations_per_hundred'] > 0) vaccinatedSouthAmerica.push(vaccinated);
        }
        else if(value['continent'] === 'Europe') {
          if(value['total_cases_per_million'] > 0) confirmEurope.push(confirm);
          if(value['total_deaths_per_million'] > 0) deathEurope.push(death);
          if(value['total_vaccinations_per_hundred'] > 0) vaccinatedEurope.push(vaccinated);
        }
        else if(value['continent'] === 'Oceania') {
          if(value['total_cases_per_million'] > 0) confirmOceania.push(confirm);
          if(value['total_deaths_per_million'] > 0) deathOceania.push(death);
          if(value['total_vaccinations_per_hundred'] > 0) vaccinatedOceania.push(vaccinated);
        }
        else {
          if(value['total_cases_per_million'] > 0) confirmContinent.push(confirm);
          if(value['total_deaths_per_million'] > 0) deathContinent.push(death);
          if(value['total_vaccinations_per_hundred'] > 0) vaccinatedContinent.push(vaccinated);
        }
      }
    }
    // Confirm
    this.dataViewConfirm.push({ name: 'Asia', series: confirmAsia });
    this.dataViewConfirm.push({ name: 'Africa', series: confirmAfrica });
    this.dataViewConfirm.push({ name: 'North America', series: confirmNorthAmerica });
    this.dataViewConfirm.push({ name: 'South America', series: confirmSouthAmerica });
    this.dataViewConfirm.push({ name: 'Europe', series: confirmEurope });
    this.dataViewConfirm.push({ name: 'Oceania', series: confirmOceania });
    // this.dataViewConfirm.push({ name: 'Continents', series: confirmContinent });
    // Death
    this.dataViewDeath.push({ name: 'Asia', series: deathAsia });
    this.dataViewDeath.push({ name: 'Africa', series: deathAfrica });
    this.dataViewDeath.push({ name: 'North America', series: deathNorthAmerica });
    this.dataViewDeath.push({ name: 'South America', series: deathSouthAmerica });
    this.dataViewDeath.push({ name: 'Europe', series: deathEurope });
    this.dataViewDeath.push({ name: 'Oceania', series: deathOceania });
    // this.dataViewDeath.push({ name: 'Continents', series: deathContinent });
    // Vaccinated
    this.dataViewVaccinated.push({ name: 'Asia', series: vaccinatedAsia });
    this.dataViewVaccinated.push({ name: 'Africa', series: vaccinatedAfrica });
    this.dataViewVaccinated.push({ name: 'North America', series: vaccinatedNorthAmerica });
    this.dataViewVaccinated.push({ name: 'South America', series: vaccinatedSouthAmerica });
    this.dataViewVaccinated.push({ name: 'Europe', series: vaccinatedEurope });
    this.dataViewVaccinated.push({ name: 'Oceania', series: vaccinatedOceania });
    // this.dataViewVaccinated.push({ name: 'Continents', series: vaccinatedContinent });
  }

  setCountryFilter() {
    this.filterCountries = '';
    for(let index = 0; index < this.countries.length; index++) {
      let value = this.countries[index];
      if(value.selected) this.filterCountries += '[' + value.name + ']';
    }
  }

  handleCountryChange() {
    this.setCountryFilter();
    this.getData();
  }

  handleRangeChange() {
    this.setDate();
    this.getData();
  }

  handleDateSubmit() {
    this.filterDate = new Date(this.filterInputDate + ' 00:00:00');
    this.getData();
  }

  setDate() {
    this.filterDate = this.dateRange[this.filterDateRange];
    this.filterInputDate = this.formatInputDate(this.filterDate);
  }

  setFilters() {
    this.rangeMax = this.dateRange.length - 1;
    this.filterDateRange = this.rangeMax;
  }

  changeFilters() {
    this.setFilterCountry();
    this.getData();
  }

  changeClearAll() {
    this.filterCountries = '';
    for(let index = 0; index < this.countries.length; index++) {
      let value = this.countries[index];
      value.selected = false;
    }
  }

  changeSelectAll() {
    this.filterCountries = '';
    for(let index = 0; index < this.countries.length; index++) {
      let value = this.countries[index];
      value.selected = true;
      this.filterCountries += '[' + value.name + ']';
    }
  }

  setFilterCountry() {
    this.filterCountries = '';
    for(let index = 0; index < this.countries.length; index++) {
      let value = this.countries[index];
      if(value.selected) this.filterCountries += '[' + value.name + ']';
    }
  }

  formatDate(date: any) {
    return Utils.formatDate(date);
  }

  formatInputDate(date: any) {
    return Utils.formatInputDate(date);
  }

}
