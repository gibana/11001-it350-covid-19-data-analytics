import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionAlphaComponent } from './section-alpha.component';

describe('SectionAlphaComponent', () => {
  let component: SectionAlphaComponent;
  let fixture: ComponentFixture<SectionAlphaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionAlphaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionAlphaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
