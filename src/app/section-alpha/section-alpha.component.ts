import { Component, OnInit, Input } from '@angular/core';
import { Utils } from '../../utils/Utils';

@Component({
  selector: 'section-alpha',
  templateUrl: './section-alpha.component.html',
  styleUrls: ['./section-alpha.component.css']
})
export class SectionAlphaComponent implements OnInit {

  @Input() data: any = [];
  @Input() dateRange: any = [];

  rangeMin: number = 0;
  rangeMax: number = 10;
  filterDateRange: number = 0;
  filterInputDate: any;
  filterDate: any;
  dataViewConfirm: any = [];
  dataViewDeath: any = [];
  dataViewVaccinated: any = [];

  view: any[] = [1000, 400];
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Countries';
  showYAxisLabel = true;

  constructor() { }

  ngOnInit(): void {
    this.setFilters();
    this.handleRangeChange();
  }

  getTotalCasesSorted() {
    let _this = this;
    let temp = Object.assign([], this.data);
    this.dataViewConfirm = [];
    this.dataViewDeath = [];
    this.dataViewVaccinated = [];
    for(let index = 0; index < temp.length; index++) {
      let value = temp[index];
      if(value['location'] === 'World' || value['location'] === 'Asia' || 
        value['location'] === 'North America' || value['location'] === 'South America' || 
        value['location'] === 'Europe' || value['location'] === 'European Union' || 
        value['location'] === 'Africa' || value['location'] === 'Oceania') continue;

      if(Date.parse(value['date']) == Date.parse(_this.filterDate)) {
        this.dataViewConfirm.push({
          name: value['location'],
          value: value['total_cases'],
        });
        this.dataViewDeath.push({
          name: value['location'],
          value: value['total_deaths'],
        });
        this.dataViewVaccinated.push({
          name: value['location'],
          value: value['total_vaccinations'],
        });
      }
    }
    this.dataViewConfirm.sort(function(a: any, b: any) { return b.value - a.value; });
    this.dataViewDeath.sort(function(a: any, b: any) { return b.value - a.value; });
    this.dataViewVaccinated.sort(function(a: any, b: any) { return b.value - a.value; });

    let limit = 25;
    this.dataViewConfirm = this.dataViewConfirm.slice(0, limit);
    this.dataViewDeath = this.dataViewDeath.slice(0, limit);
    this.dataViewVaccinated = this.dataViewVaccinated.slice(0, limit);
  }

  handleRangeChange() {
    this.setDate();
    this.getTotalCasesSorted();
  }

  handleDateSubmit() {
    this.filterDate = new Date(this.filterInputDate + ' 00:00:00');
    this.getTotalCasesSorted();
  }

  handleChartSelect(event: any) {
    console.log(event);
  }

  setDate() {
    this.filterDate = this.dateRange[this.filterDateRange];
    this.filterInputDate = this.formatInputDate(this.filterDate);
  }

  setFilters() {
    this.rangeMax = this.dateRange.length - 1;
    this.filterDateRange = this.rangeMax;
  }

  formatDate(date: any) {
    return Utils.formatDate(date);
  }

  formatInputDate(date: any) {
    return Utils.formatInputDate(date);
  }

}
