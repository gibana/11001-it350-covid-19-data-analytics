import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/DataService'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  data: any = [];
  dateRange: any = [];
  countries: any = [];
  countriesPopulation: any = [];
  countriesEconomy: any = [];
  isDataLoaded: boolean = false;

  sectionAlpha: boolean = false;
  sectionBeta: boolean = false;
  sectionCharlie: boolean = false;
  sectionDelta: boolean = false;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.getData();
    this.setVisible();
  }

  setVisible() {
    let _this = this;
    _this.sectionAlpha = true;
    setTimeout(function(){
      _this.sectionBeta = true;
    }, 5000);
  }

  population() {
    let _this = this;
    _this.sectionCharlie = true;
  }

  economics() {
    let _this = this;
    _this.sectionDelta = true;
  }

  getData() {
    this.dataService.get().subscribe(data => {
      let list = data.split('\n');
      let columns = [];
      list.forEach(item => {
        let row = item.split(',');
        if(columns.length === 0) {
          columns = row;
        } else {
          let record = {};
          for(let index = 0; index < columns.length; index++) {
            let value = row[index];
            let column = columns[index];
            if(column === 'date') {
              record[column] = new Date(value + ' 00:00:00');
            } else {
              record[column] = value !== '' ? value : 0;
              if(!isNaN(record[column])) {
                record[column] = parseFloat(record[column]);
              }
            }
          }
          this.data.push(record);
          this.dateRange.push(record['date']);
          let selected = false;
          let name = record['location'];
          if(name === 'Africa' || name === 'North America' || name === 'South America' || 
            name === 'Europe' || name === 'Asia' || name === 'Oceania') selected = true;
          this.countries.push({ name: name, selected: selected });
          this.countriesPopulation.push({ name: name, selected: selected });
          this.countriesEconomy.push({ name: name, selected: selected });
        }
      });
      this.dateRange = this.dateRange
        .map(function (date: any) { return date.getTime() })
        .filter(function (date: any, i: any, array: any) { return array.indexOf(date) === i; })
        .map(function (time: any) { return new Date(time); });
      this.countries = [...new Map(this.countries.map((item: any) => [item['name'], item])).values()];
      this.countriesPopulation = [...new Map(this.countriesPopulation.map((item: any) => [item['name'], item])).values()];
      this.countriesEconomy = [...new Map(this.countriesEconomy.map((item: any) => [item['name'], item])).values()];
      this.dateRange.sort(function(a: any, b: any){
        let dateFirst: any = new Date(a);
        let dateSecond: any = new Date(b);
        return dateFirst - dateSecond;
      });
      this.isDataLoaded = true;
    });
  }

}
