import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionBetaComponent } from './section-beta.component';

describe('SectionBetaComponent', () => {
  let component: SectionBetaComponent;
  let fixture: ComponentFixture<SectionBetaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionBetaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionBetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
