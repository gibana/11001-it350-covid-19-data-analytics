import { Component, OnInit, Input } from '@angular/core';
import { Utils } from '../../utils/Utils';

@Component({
  selector: 'section-beta',
  templateUrl: './section-beta.component.html',
  styleUrls: ['./section-beta.component.css']
})
export class SectionBetaComponent implements OnInit {
  
  @Input() data: any = [];
  @Input() dateRange: any = [];
  @Input() countries: any = [];

  dataViewConfirm: any = [];
  dataViewDeath: any = [];
  dataViewVaccinated: any = [];
  dataViewFullyVaccinated: any = [];

  filterCountries: string = '';

  view: any[] = [1000, 400];
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Date';
  timeline: boolean = true;

  constructor() { }

  ngOnInit(): void {
    this.changeFilters();
  }

  changeFilters() {
    this.setFilters();
    this.getData();
  }

  setFilters() {
    this.filterCountries = '';
    for(let index = 0; index < this.countries.length; index++) {
      let value = this.countries[index];
      if(value.selected) this.filterCountries += '[' + value.name + ']';
    }
  }

  getData() {
    let temp = Object.assign([], this.data);
    let name = '';

    let seriesConfirm = [];
    let seriesDeath = [];
    let seriesVaccinated = [];
    let seriesFullyVaccinated = [];

    let maxConfirm = 0;
    let maxDeath = 0;
    let maxVaccinated = 0;
    let maxFullyVaccinated = 0;
    this.dataViewConfirm = [];
    this.dataViewDeath = [];
    this.dataViewVaccinated = [];
    this.dataViewFullyVaccinated = [];

    for(let index = 0; index < temp.length; index++) {
      let value = temp[index];
      if(!this.filterCountries.includes('[' + value['location'] +']')) continue;
      if(name !== value['location']) {
        name = value['location'];
        seriesConfirm = [];
        seriesDeath = [];
        seriesVaccinated = [];
        seriesFullyVaccinated = [];
        maxConfirm = 0;
        maxDeath = 0;
        maxVaccinated = 0;
        maxFullyVaccinated = 0;
        if(name !== '') {
          this.dataViewConfirm.push(this.setItem(name, seriesConfirm));
          this.dataViewDeath.push(this.setItem(name, seriesDeath));
          this.dataViewVaccinated.push(this.setItem(name, seriesVaccinated));
          this.dataViewFullyVaccinated.push(this.setItem(name, seriesFullyVaccinated));
        }
      } else {
        if(maxConfirm === 0 || value['total_cases'] > 0) {
          seriesConfirm.push({ name: value['date'], value: value['total_cases'] });
          maxConfirm = value['total_cases'];
        }
        if(maxDeath === 0 || value['total_deaths'] > 0) {
          seriesDeath.push({ name: value['date'], value: value['total_deaths'] });
          maxDeath = value['total_deaths'];
        }
        if(maxVaccinated === 0 || value['total_vaccinations'] > 0) {
          seriesVaccinated.push({ name: value['date'], value: value['total_vaccinations'] });
          maxVaccinated = value['total_vaccinations'];
        }
        if(maxFullyVaccinated === 0 || value['people_fully_vaccinated'] > 0) {
          seriesFullyVaccinated.push({ name: value['date'], value: value['people_fully_vaccinated'] });
          maxFullyVaccinated = value['people_fully_vaccinated']; 
        }
      }
    }
    // this.dataViewConfirm.push(this.setItem(name, seriesConfirm));
    // this.dataViewDeath.push(this.setItem(name, seriesDeath));
    // this.dataViewVaccinated.push(this.setItem(name, seriesVaccinated));
    // this.dataViewFullyVaccinated.push(this.setItem(name, seriesFullyVaccinated));
  }

  setItem(name: string, series: any[]) {
    series = series.sort(function(a: any, b: any){
      let dateFirst: any = new Date(a.name);
      let dateSecond: any = new Date(b.name);
      return dateFirst - dateSecond;
    });
    return { name: name, series: series };
  }
}
